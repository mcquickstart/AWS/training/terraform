param(
    # Parameter help description
    [Parameter(Mandatory)]
    [string]
    $CommitMessage
)
git branch

Write-Host "Running git pull"
git pull

Write-Host "Starting  git add dry run"
git add -n .
$Confirm = Read-Host -Prompt "Do you want to commit the above changes?(Yes/No)?"

if($Confirm.ToUpper() -eq "YES") {
    Write-Host "Starting  git add"
    git add .

    Write-Host "Starting  git commit"
    git commit -m $CommitMessage

    Write-Host "Starting  git push"
    git push
    Write-Host "Success pushed to the current branch"
}
else {
    Write-Host "Changes are not pushed. "
}