# Instructions

This ReadMe file is a guideline to write the terraform code for these project. It defines what standards needs to be follwoed and maintained throughout the entire project.


Let's begin.

## Mandatory rules
---
1. Do not store any ***sensitve information*** in the repository as this can be viewed and used maliciously. For example, 
   - User name and password
   - Database password
   - Application key
   - Auhenticaion token
   - PII data

2. Implement ***least access priviledge*** as much as possible.

## Important rules to follow
---
1. Maintain the following folder structure
   - **modules**: Keep the codes for the modules here.
   - **environments**: keep different environments here. Typically, the environemnts are **dev**, **qa** and **prod**.
2. Make sure that all the resources have proper tags. In this project the following tags are mandatory.
   - Environment
   - Application
   - Automation
   - Cost
   - KeepAlive
   - Name
If any of the tags are missing, then the code will not execute.
3. Try to add validation as much as possible in the code.
4. Mention the dependencies as much as possible.

## Common code block
---
Please keep the code block in all variables.tf files.

```terraform
locals {
    common_tags = {
        "Environment" = var.environemnt, 
        "Application" = var.application,
        "Automation" = "True"
    }

    free_tags = {
        "Cost" = "Free"
    }
    
    keep_alive_tags = {
        "KeepAlive" = "True"
    }
}

variable "environemnt" {
    type = string
    description = "Specify the environment - dev, qa or production"
    validation {
        condition = contains(["dev", "qa", "prod"], lower(var.environemnt))
        error_message = "Environment should one of dev, qa and prod"
    }
    default = "dev"
}

variable "application" {
    type = string
    description = "Specify the name of the application"
    default = "Training"
}
```

In each modules the following tags must be attached - 

```terraform
tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        resourc_specific_tags,
        name_tag
    )
```
