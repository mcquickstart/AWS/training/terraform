#Create the training VPC.
resource "aws_vpc" "application_vpc" {
    cidr_block = var.cidr_block
    tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.vpc_tags,
        {"Name" = "${var.application}-${var.environemnt}-vpc"}
    )
    enable_dns_hostnames = var.vpc_enable_dns_hostname
}