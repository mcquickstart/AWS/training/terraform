resource "aws_subnet" "public_subnets" {
    # var.public_subnet_details is a list, so we must now project it into a map
    # where each key is unique. Here we used availability zones as unique key.
    for_each = {
        for subnet in var.public_subnet_details : "${subnet.az}" => subnet
    }
    vpc_id = aws_vpc.application_vpc.id
    cidr_block = each.value.cidr_block
    availability_zone = each.value.az
    tags = merge(
                local.common_tags,
                local.free_tags,
                local.keep_alive_tags,
                each.value.tags,
                {"Name" = "${var.application}-${var.environemnt}-${each.value.name}"}
            )
    depends_on = [
      aws_vpc.application_vpc
    ]
}

resource "aws_subnet" "private_subnets" {
    # var.private_subnet_details is a list, so we must now project it into a map
    # where each key is unique. Here we used availability zones as unique key.
    for_each = {
        for subnet in var.private_subnet_details : "${subnet.az}" => subnet
    }

    vpc_id = aws_vpc.application_vpc.id
    cidr_block = each.value.cidr_block
    availability_zone = each.value.az
    tags = merge(
                local.common_tags,
                local.free_tags,
                local.keep_alive_tags,
                each.value.tags,
                {"Name" = "${var.application}-${var.environemnt}-${each.value.name}"}
            )
    depends_on = [
      aws_vpc.application_vpc
    ]
}