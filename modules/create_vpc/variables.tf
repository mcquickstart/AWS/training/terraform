locals {
    common_tags = {
        "Environment" = var.environemnt, 
        "Application" = var.application,
        "Automation" = "True"
    }

    free_tags = {
        "Cost" = "Free"
    }
    
    keep_alive_tags = {
        "KeepAlive" = "True"
    }
}

variable "environemnt" {
    type = string
    description = "Specify the environment - dev, qa or production"
    validation {
        condition = contains(["dev", "qa", "prod"], lower(var.environemnt))
        error_message = "Environment should one of dev, qa and prod"
    }
    default = "dev"
}

variable "application" {
    type = string
    description = "Specify the name of the application"
    default = "Training"
}

variable "cidr_block" {
    type = string
    description = "Specify the CIDR block for the VPC"
}

variable "vpc_tags" {
    type = map(string)
    description = "Tags for the VPC."
    default = { }
}

variable "ig_tags" {
    type = map(string)
    description = "Tags for the Internet gateway."
    default = { }
}

variable "vpc_enable_dns_hostname" {
    type = bool
    description = "Specify whether to enable DNS host name"
    default = true
}

variable "public_subnet_details" {
    type=list(object ({
        cidr_block = string,
        az = string,
        tags = map(string),
        name = string
    }))
    description = "Specify the public subnet details"
}

variable "private_subnet_details" {
    type=list(object ({
        cidr_block = string,
        az = string,
        tags = map(string),
        name = string
    }))
    description = "Specify the private subnet details"
}

variable "public_route_table_tags" {
    type = map(string)
    description = "Tags for public route tables"
    default = { }
}

variable "private_route_table_tags" {
    type = map(string)
    description = "Tags for private route tables"
    default = { }
}