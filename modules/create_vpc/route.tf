resource "aws_route_table" "public_rt" {
    vpc_id = aws_vpc.application_vpc.id
    tags = merge (
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.public_route_table_tags,
        {"Name" = "${var.application}-${var.environemnt}-public-rt"}
    )
}

resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.application_vpc.id
    tags = merge (
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.private_route_table_tags,
        {"Name" = "${var.application}-${var.environemnt}-private-rt"}
    )
}

resource "aws_route" "internet_route" {
    route_table_id = aws_route_table.public_rt.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.public_ig.id
    depends_on = [
        aws_vpc.application_vpc,
        aws_internet_gateway.public_ig,
        aws_subnet.public_subnets,
        aws_route_table.public_rt
    ]
}

resource "aws_route_table_association" "public_rta" {
    for_each = aws_subnet.public_subnets
    subnet_id = each.value.id
    route_table_id = aws_route_table.public_rt.id

    depends_on = [
        aws_vpc.application_vpc,
        aws_internet_gateway.public_ig,
        aws_subnet.public_subnets,
        aws_route_table.public_rt
    ]
}

resource "aws_route_table_association" "private_rta" {
    for_each = aws_subnet.private_subnets
    subnet_id = each.value.id
    route_table_id = aws_route_table.private_rt.id

    depends_on = [
        aws_vpc.application_vpc,
        aws_subnet.private_subnets,
        aws_route_table.private_rt
    ]
}