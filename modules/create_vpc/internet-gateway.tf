resource "aws_internet_gateway" "public_ig" {
    vpc_id = aws_vpc.application_vpc.id
    tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.ig_tags,
        {"Name" = "${var.application}-${var.environemnt}-ig"})
    depends_on = [
      aws_vpc.application_vpc
    ]
}