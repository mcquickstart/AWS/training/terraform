locals {
    common_tags = {
        "Environment" = var.environemnt, 
        "Application" = var.application,
        "Automation" = "True"
    }

    free_tags = {
        "Cost" = "Free"
    }
    
    keep_alive_tags = {
        "KeepAlive" = "True"
    }
}

variable "environemnt" {
    type = string
    description = "Specify the environment - dev, qa or production"
    validation {
        condition = contains(["dev", "qa", "prod"], lower(var.environemnt))
        error_message = "Environment should one of dev, qa and prod"
    }
    default = "dev"
}

variable "application" {
    type = string
    description = "Specify the name of the application"
    default = "Training"
}

variable "vpc_id" {
    type = string
    description = "Specify the VPC Id"
}

variable "app_sg_name" {
    type = string
    description = "Specify the name of the security group"
}

variable "app_sg_tags" {
    type =map(string)
    description = "Tags for Security group"
    default = { }
}

variable "app_sg_ssh_cidr" {
    type = list(string)
    description = "Specify the SSH connection CIDR"
}

variable "app_sg_http_cidr" {
    type = list(string)
    description = "Specify the HTTP connection CIDR"
}