resource "aws_security_group" "app_sg" {
    name = var.app_sg_name
    description = "Application server security group"
    vpc_id = var.vpc_id
    tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.app_sg_tags,
        {"Name" = "${var.application}-${var.environemnt}-app_sg"})
}

resource "aws_security_group_rule" "app_sg_ingress_r1" {
      type              = "ingress"
      from_port         = 22
      to_port           = 22
      protocol          = "tcp"
      cidr_blocks       = var.app_sg_ssh_cidr
      security_group_id = aws_security_group.app_sg.id
      description       = "SSH connection from outside world."
}

resource "aws_security_group_rule" "app_sg_ingress_r2" {
      type              = "ingress"
      from_port         = 80
      to_port           = 80
      protocol          = "tcp"
      cidr_blocks       = var.app_sg_http_cidr
      security_group_id = aws_security_group.app_sg.id
      description       = "HTTP connection from outside world."
}

resource "aws_security_group_rule" "app_sg_ingress_r3" {
      type              = "ingress"
      from_port         = 443
      to_port           = 443
      protocol          = "tcp"
      cidr_blocks       = var.app_sg_http_cidr
      security_group_id = aws_security_group.app_sg.id
      description       = "HTTPS connection from outside world."
}