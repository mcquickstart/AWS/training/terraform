resource "aws_network_interface" "app_server_eni" {
    subnet_id = var.subnet_id
    security_groups = [data.aws_security_group.app_sg.id]
    tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.eni_tags,
        {"Name" = "${var.application}-${var.environemnt}-eni"}
    )
}