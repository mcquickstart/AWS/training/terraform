resource "aws_instance" "app_server" {
    ami = var.ami_id
    instance_type = var.instance_type
    key_name = var.key_pair_name
    subnet_id = var.subnet_id
    depends_on = [
      aws_ebs_volume.app_server_vol1,
      aws_network_interface.app_server_eni
    ]
    security_groups = [ "${var.app_server_sg}" ]
    ebs_block_device {
      
    }
}