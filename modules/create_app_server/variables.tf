locals {
    common_tags = {
        "Environment" = var.environemnt, 
        "Application" = var.application,
        "Automation" = "True"
    }

    free_tags = {
        "Cost" = "Free"
    }
    
    keep_alive_tags = {
        "KeepAlive" = "True"
    }
}

variable "environemnt" {
    type = string
    description = "Specify the environment - dev, qa or production"
    validation {
        condition = contains(["dev", "qa", "prod"], lower(var.environemnt))
        error_message = "Environment should one of dev, qa and prod"
    }
}

variable "application" {
    type = string
    description = "Specify the name of the application"
}

variable "volume_details" {
    type = object({
            size = optional(number, 1),
            type = optional(string, "gp2"),
            encrypted = optional(bool,  false),
            tags = optional(map(string), { })
        }
    )

    description = "Specify the volume details"
    default = { }
}

variable "availability_zone" {
    type = string
    description = "Specify the avaiability zone"
    default = "ap-south-1a"
}

variable "subnet_id" {
    type=string
    description = "Specify the subnet id of the subnet where the resources will be created"
}

variable "eni_ip" {
    type = string
    description = "Specify the IP of the ENI"
    default = ""
}

variable "eni_tags" {
  type = map(string)
  description = "Specify the tags"
  default = {  }
}

variable "ami_id" {
    type=string
    description = "Specify the AMI id"
}

variable "instance_type" {
    type = string
    description = "Specify the instance type."
}

variable "key_pair_name" {
    type= string
    description = "Specify the key pair"
}

variable "app_server_sg" {
    type = string
    description = "Security groups for the ENI and application server"
}