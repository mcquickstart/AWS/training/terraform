resource "aws_ebs_volume" "app_server_vol1" {
    size = var.volume_details.size
    type = var.volume_details.type
    encrypted = var.volume_details.encrypted
    availability_zone = var.availability_zone
    tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        var.volume_details.tags,
        {"Name" = "${var.application}-${var.environemnt}-vol1"}
    )
}

resource "aws_volume_attachment" "app_server_vol1_attachment" {
  device_name = "/dev/sdh"
  volume_id = aws_ebs_volume.app_server_vol1.id
  instance_id = aws_instance.app_server.id
  depends_on = [
    aws_instance,
    aws_ebs_volume
  ]
}

