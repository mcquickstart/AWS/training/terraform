#!/bin/bash
set -e
TEMP=`getopt -o e:c:m: --long environment:,command:,module: -- "$@"`
eval set --  "$TEMP"

while true ; do
    case "$1" in
        -e|--environment) build_environment="$2"; shift 2;;
        -c|--command) build_command="$2"; shift 2;;
        -m|--module) build_module="$2"; shift 2;;
        --) shift; break;;
    esac
done

cd ../environments/$build_environment/$build_module/

terraform init

case $build_command in
    plan)           terraform plan -out terraform.tfplan;
                    echo "Terraform plan executed and saved";
                    ;;
    destroy-plan)   terraform plan -destroy -out terraform.tfplan;
                    echo "Terraform destroy plan executed and saved";
                    ;;
    apply)          terraform apply terraform.tfplan;
                    echo "Terraform plan applied";
                    ;;
    destroy-apply)  terraform apply -destroy terraform.tfplan;
                    echo "Terraform Destroy plan applied";
                    ;;
    build)          terraform plan -out terraform.tfplan;
                    terraform apply terraform.tfplan
                    echo "Executed terraform plan and applied";
                    ;;
    destroy)        terraform plan -destroy -out terraform.tfplan;
                    terraform apply -destroy terraform.tfplan;
                    echo "Executed terraform destroy plan and applied";
                    ;;
esac