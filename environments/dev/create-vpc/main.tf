module "create_dev_vpc" {
  source = "../../../../modules/create_vpc"
  application = ""
  environemnt = ""
  cidr_block = ""

  public_subnet_details = [
    {
      cidr_block=""
      az = ""
      tags = { },
      name = ""
    },
    {
      cidr_block=""
      az = ""
      tags = { },
      name = ""
    },
    {
      cidr_block=""
      az = ""
      tags = { },
      name = ""
    }
  ]

  private_subnet_details = [
    {
      cidr_block=""
      az = ""
      tags = { },
      name = ""
    },
    {
      cidr_block="10.0.12.0/24"
      az = ""
      tags = { },
      name = ""
    },
    {
      cidr_block="10.0.13.0/24"
      az = ""
      tags = { },
      name = ""
    }
  ]
}