module "create_dev_app_server" {
  source = "../../../../modules/create_app_server"
  application = ""
  environemnt = ""
  availability_zone = ""
  subnet_id = ""
  app_server_sg = ""  
  ami_id = ""
  instance_type = "t2.micro"
  key_pair_name = ""
}